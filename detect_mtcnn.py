import numpy as np
import PIL

def face_detect(mtcnn,img):
    boxes,_=mtcnn.detect(img)
    if boxes is None:
        return None,None,0,0,0,0
    box=boxes[0]
    l,t,r,b=int(box[0]),int(box[1]),int(box[2]),int(box[3])
    width,height=img.size
    scale_l=0.4
    scale_r=0.4
    scale_t=0.3
    scale_b=0.2
    w,h=r-l,b-t
    if(t-scale_t*h<0):
        t=1
    else:
        t-=int(scale_t*h)
    if(b+scale_b*h>height):
        b=height
    else:
        b+=int(scale_b*h)
    if(l-scale_l*w>0):
        l-=int(scale_l*w)
    else:
        l=1
    if(r+scale_r*w<width):
        r+=int(scale_r*w)
    else:
        r=width
    quad=np.array([[l,t],[l,b],[r,b],[r,t]])
    img_result=img.transform((512,512),PIL.Image.QUAD, (quad + 0.5).flatten(),
                            PIL.Image.BILINEAR)
    return img_result,box,l,t,r,b
