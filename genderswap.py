import imageio
from PIL import Image
import cv2
import numpy as np
import dlib
import torch
import torchvision.transforms as transforms
import time
from argparse import Namespace

from pix2pixHD.models.networks import define_G
import pix2pixHD.util.util as util
from detect_mtcnn import face_detect
from facenet_pytorch import MTCNN
from FaceSwap.face_detection import select_face_with_landmarks
from FaceSwap.face_swap import face_swap

def get_eval_transform():
    transform_list = []
    transform_list += [transforms.Normalize((0.5, 0.5, 0.5),
                                            (0.5, 0.5, 0.5))]
    return transforms.Compose(transform_list)

transform = get_eval_transform()
config_G = {
    'input_nc': 3,
    'output_nc': 3,
    'ngf': 64,
    'netG': 'global',
    'n_downsample_global': 4,
    'n_blocks_global': 9,
    'n_local_enhancers': 1,
    'norm': 'instance',
}
def male2female(model,img,aligned_img,l,t,r,b):
    img_np=np.array(img)
    aligned_img=transforms.ToTensor()(aligned_img)
    img_transform = transform(aligned_img).unsqueeze(0)
    with torch.no_grad():
        img_result = model(img_transform.cuda())
        
    img_result = util.tensor2im(img_result.data[0])
    img_result=cv2.resize(img_result,(r-l,b-t))
    fore_ground=np.ones((img_result.shape[0],img_result.shape[1],3))
    fore_ground*=255
    fore_ground=np.uint8(fore_ground)
    center = (int(img_result.shape[1]/2+l), int(img_result.shape[0]/2+t))
#     print(center)
#     img_result=cv2.imread("test/src.jpg")
#     img_np=cv2.imread("test/dest.jpg")
#     fore_ground=cv2.imread("test/mask.jpg")
    start = time.time()
    result=cv2.seamlessClone(img_result, img_np, fore_ground, center, cv2.NORMAL_CLONE)
#     cv2.imwrite("test/src.jpg",cv2.cvtColor(img_result, cv2.COLOR_BGR2RGB))
#     cv2.imwrite("test/dest.jpg",cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB))
#     cv2.imwrite("test/mask.jpg",cv2.cvtColor(fore_ground, cv2.COLOR_BGR2RGB))
    end = time.time()
    print("Swap time: {}".format(end - start))
    return result
def female2male(model, img, aligned_img, predictor, box,l,t,r,b):
    
    img_np=np.array(img)
    aligned_img=transforms.ToTensor()(aligned_img)
    img_transform = transform(aligned_img).unsqueeze(0)
    with torch.no_grad():
        img_result = model(img_transform.cuda())
    img_result = util.tensor2im(img_result.data[0])
    faceswap_img = get_faceswap(img, img_result, predictor, box, l, t, r, b)
    return faceswap_img
def get_faceswap(original_img, genderswap_img, predictor, box, l, t, r, b):
    src_img = cv2.cvtColor(np.array(genderswap_img), cv2.COLOR_RGB2BGR)
    dst_img = cv2.cvtColor(np.array(original_img), cv2.COLOR_RGB2BGR)
    
    original_landmarks = get_landmarks(original_img, box, predictor)
    aligned_landmarks = get_aligned_landmarks(original_landmarks, l, t, r, b)
    
    src_points, src_shape, src_face = select_face_with_landmarks(src_img, aligned_landmarks)
    dst_points, dst_shape, dst_face = select_face_with_landmarks(dst_img, original_landmarks)

    args = Namespace(correct_color=True, warp_2d=False)
    output = face_swap(src_face, dst_face, src_points, dst_points, dst_shape, dst_img, args)
    return Image.fromarray(cv2.cvtColor(output, cv2.COLOR_BGR2RGB))

def get_landmarks(original_img, box, predictor):
    img = np.array(original_img)
    mtcnn_box = dlib.rectangle(box[0], box[1], box[2], box[3])
    shape = predictor(img, mtcnn_box)
    coords = np.asarray(list([p.x, p.y] for p in shape.parts()), dtype=np.int)
    return coords
def get_aligned_landmarks(landmarks, l, t, r, b):
    aligned_landmarks = []
    old_w, old_h = r - l, b - t
    old_center_x, old_center_y = old_w/2.0, old_h/2.0
    scale_w, scale_h = 512.0 / old_w, 512.0 / old_h
    for p in landmarks:
        old_x = p[0] - (l + 0.5)
        old_y = p[1] - (t + 0.5)
        aligned_p = [256 + (old_x - old_center_x) * scale_w, 256 + (old_y - old_center_y) * scale_h]
        aligned_landmarks.append(aligned_p)
    return np.asarray(aligned_landmarks, dtype=np.int)

def gender_swap(model,mtcnn,predictor,img_filename, gender):
    img=Image.open(img_filename)
    start = time.time()
    aligned_img,box,l,t,r,b=face_detect(mtcnn,img)
#     gender="male"
    
    if(gender=="male"):
        #male to female
        result=male2female(model,img,aligned_img,l,t,r,b)
    else:
        #female to male
        result=female2male(model, img, aligned_img, predictor, box,l,t,r,b)
    end = time.time()
    print("Total time: {}".format(end - start))
    return result
def main():
    #load male to female model
    gender = "female"
    weights_path = "female2male.pth"
#     weights_path = "male2female.pth" if gender == "male" else "female2male.pth"
    landmarks_path = "shape_predictor_68_face_landmarks.dat"
    gender_swap_model = define_G(**config_G)
    pretrained_dict = torch.load(weights_path)
    gender_swap_model.load_state_dict(pretrained_dict)
    gender_swap_model = gender_swap_model.cuda()
    #load face detection model
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print('Running on device: {}'.format(device))
    face_detect_model = MTCNN(keep_all=True, device=device)
    predictor = dlib.shape_predictor(landmarks_path)
    #gender swap
#     result=gender_swap(gender_swap_model,face_detect_model,"male1/giang.jpg")
#     imageio.imsave('result1/giang.jpg', result)
    file_name = "female2"
    input_path = "data/{}.jpeg".format(file_name)
    output_path = "results/{}_faceswap.jpeg".format(file_name)
    result=gender_swap(gender_swap_model,face_detect_model,predictor,input_path, gender)
    imageio.imsave(output_path, result)
    print("Results saved in {}".format(output_path))
if __name__ == "__main__":
    main()